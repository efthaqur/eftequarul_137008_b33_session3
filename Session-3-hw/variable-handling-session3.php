<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 9/21/2016
 * Time: 9:29 AM
 */

    $string_with_float="235.269bitm45";
    $proDisplay;

    echo floatval($string_with_float)."<br />"; # floatval shows the float value first portion of variable

    var_dump(empty ($proDisplay))."<br />"; # empty() returns a boolean value about variable empty or not

?>

<br />

<?php

        $a = array('5','7','9','11','13');
        $b = 2588;
                                    // is_array shows a variable array or not, returns boolean value!
        var_dump(is_array($a));

        echo "<br />";

        var_dump(is_array($b));

        $c;
        $d = '';
        $e = "";

        var_dump(is_null($c)); //Displays variable null or not
        echo "<br />";
        var_dump(is_null($d));
        echo "<br />";
        var_dump(is_null($e));
        echo "<br />";

?>
<?php
    echo "<br />";
    $var1 = 25;
    $var2;
    $var3 = "";
    $a = array('5','71','9','11','13');

    var_dump(isset($var1)); // example of isset() function
    echo "<br />";
    var_dump(isset($var2));
    echo "<br />";
    var_dump(isset($var3));
    echo "<br />";
    print_r($a);  //showing value of array a using print_r
    echo "<br />";
    echo $a;        //difference between echo and print_r
    echo "<br />";
    var_dump($a);    // difference between print_r and var_dump

    echo "<br />";
    echo serialize($a); // shows the index and string length of a array, a serial view of array
    echo "<br />";
    $var4 = serialize($a);
    print_r( unserialize($var4)); //revert of serialize

    $varForunset = 125; #define a varibale and its value
    echo "<br />";
    echo $varForunset; #prints the value of variable
    echo "<br />";
    unset($varForunset); #value of the variable cleared in this line
    echo  $varForunset;

    var_export($var1); //Example of var_export funtion
    echo "<br />";
    var_export($varForunset); //checking last variable which value was unset
    echo "<br />";
    var_export($a); //testing a array;
    echo "<br /><br />";

    echo gettype($a); //getting the type of an array
    echo "<br />";
    echo gettype($var1); //getting the data type of $var1
    echo gettype($varForunset); // experiment what is a variable is unset what will be the gettype
    echo "<br />";


?>

<?php
    /* This block shows the example of these function,
        is_bool
        is_float
        is_string
        is_int
        boolval
        intval
        is_object
        is_scaler
*/
        $var5 = true;
        $var6 = 23568.12;
        $var7 = "BiTM!!";
        $var8 = 248756;
        $var9 = false;

        var_dump(is_bool($var5)); #var_dump used to print data type, echo just print the value wheather var_dump prints data type
        echo "<br />";
        var_dump(is_float($var6)); #Return a boolean value true as $var6 is floats
        echo "<br />";
        var_dump(is_string($var7)); # is_string is the function which test a variable's value string or not, if string returns true else returns false
        echo "<br />";
        var_dump(is_int($var8)); //is_int is the fucntion which test if value is integer or not
        echo "<br />";
        var_dump(boolval($var5)); // Shows the boolean value variebale
        echo "<br />";
        var_dump(boolval($var9)); //shows the boolean value of variable
        echo "<br />";
        var_dump(boolval($var7)); // only 0 is false except this everything is true!
        echo "<br />";
        var_dump(intval($var6)); // prints the integer value of variable
        echo "<br />";
        var_dump(is_object($var7)); // print if value of variable object or not
        echo "<br />";
        var_dump(is_scalar($var8)); // prints if value of variable scalar or compound

?>

