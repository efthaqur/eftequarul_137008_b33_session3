<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Session 3 Practice file</title>
</head>
<body>
	<?php 
	
		//This is php file shows an example on session 3 topics

		echo "This is PHP!"." "."See the Source Code, This is standard way to write php code!!";

		/* This is multi line comment!
		 * All example of session 3 embeded here!
		 * Checkout multiple line comment!
		*/
	?>	

	<br/>

	<?
		//Example of Short tag!
		print "This is written by short tag!";


	?>
	<br />
	<?php
		echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"."<br/>"; #This is double quoted string line!
		#Here the data type php discussed	
		echo 'PHP has 4 type of scalar data type.
			  they are integer, boolean, float, string
				besides this is single quoted string line!
			  ';
	?>
	
	<br />
	<?php
		echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"."<br/>"; #This is double quoted string line!
		
		#heredoc example!
		$acer = "Example Ended";	
		$story = <<<EOD
Integer means 1,2,3,4,5,-1,-2,-3,-4, etc...
          where float means 0.2,5.6, 3e10^26 etc...
				again Boolean means True or false 
						and what are you seeing this all are strings!! $acer

EOD;
	echo $story;


		?>
			<?php
		echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"."<br/>"; #This is double quoted string line!
		
		#nowdoc example!
		$acer = "Example Ended";	
$story1 = <<<'EOD'
PHP has two compound type data,
		one is array which is a variable contains multiple values,
another is object which will be discussed later,
		PHP has also two type of special data, they are resource and NULL. $acer
EOD;
	echo $story1;


		?>
	<?php
			echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"."<br/>"; #This is double quoted string line!
		//This Blocks show you how to decalre variable!

	$acer = "Laptop";
	$hp = 56;
	$dell = 36.3351;
	$stat = true;
	$aspire = false;

		echo "Printing all variables"."<br/>";
			echo $acer."<br>".$hp."<br>".$dell."<br>".$stat."<br>".$aspire."<br>";

	?>
	<br>
	
	<?php
	    $sub = array("Physics","Chemistry","Math");

        echo "Pure science is" . " " . $sub[0] ."," . $sub[1] . "," . $sub[2] . ".";


    $x = 75;
    $y = 25;

    function addition()
    {
        $GLOBALS['z'] = $GLOBALS['x'] + $GLOBALS['y'];

    }

    addition();
    echo "<br />";
    echo $z;
    echo "<br />";
    echo $_SERVER['PHP_SELF'];
    echo "<br />";
    echo $_SERVER['REMOTE_ADDR'];
    echo "<br />";
    echo $_SERVER['HTTP_USER_AGENT']; # Shows which user agent are using!
    echo "<br />";
    
	?>
	

</body>
</html>

